﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Task24MVC.Models;

namespace Task24MVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View("CoachInfo");
        }
        [HttpGet]

        public IActionResult CoachInfo()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CoachInfo(Coach coach)
        {
            if (ModelState.IsValid)
            {
                CoachGroup.Coaches.Add(coach);

                return View("AddCoachConfirmation", coach);
            }
            else
            {
                return View();
            }

        }
        [HttpGet]
        public IActionResult AllCoaches(Coach coach)
        {
            Coach coach1 = new Coach { Name = "Marius", ID = 1, IsAvailable = true, Level = "Beginner" };
            Coach coach2 = new Coach { Name = "Henrik", ID = 2, IsAvailable = false, Level = "Pro" };
            Coach coach3 = new Coach { Name = "Pedro", ID = 3, IsAvailable = true, Level = "Pro" };

            CoachGroup.AddCoach(coach1);
            CoachGroup.AddCoach(coach2);
            CoachGroup.AddCoach(coach3);

            return View(CoachGroup.Coaches);
        }
    }
}
