﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task24MVC.Models
{
    public static class CoachGroup
    {
        private static List<Coach> CurrentCoaches = new List<Coach>();

        public static List<Coach> Coaches
        {
            get { return CurrentCoaches; }
        }

        public static void AddCoach(Coach newcoach)
        {
            CurrentCoaches.Add(newcoach);
        }
    }
}
